## English-speaking

* Facebook
    * [BadukLife][baduklife-fb]
    * [Go (Weiqi) Players][fbplayers]
* Forums
    * [Life in 19x19][l19]
    * [OGS forums][ogs]
* Google+
    * [BadukLife][baduklife-gg]
    * [The Go Community][gocommunity]
* Newsgroups
    * [rec.games.go][rec.games.go]
* Reddit
    * [/r/baduk][rbaduk]
    * [/r/cbaduk][rcbaduk]

[baduklife-fb]: https://www.facebook.com/groups/Baduklife/about/
[baduklife-gg]: https://plus.google.com/communities/116653697899556201210
[fbplayers]: https://www.facebook.com/groups/go.igo.weiqi.baduk/
[gocommunity]: https://plus.google.com/communities/116653697899556201210
[l19]: https://lifein19x19.com/
[ogs]: https://forums.online-go.com/
[rec.games.go]: https://groups.google.com/forum/#!forum/rec.games.go
[rbaduk]: http://www.reddit.com/r/baduk/
[rcbaduk]: http://www.reddit.com/r/cbaduk/
